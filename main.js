import mqtt from 'mqtt';
import fs from 'fs';
import express from 'express';
import Prometheus from 'prom-client';

const app = express();
const port = 3000;

import spotprice from './lib/spotprice.js';

import ruuviGateway from './lib/ruuvi-gateway.js';

var mqtt_broker = 'mqtt://172.16.140.5';
if (process.env['MQTT_BROKER']) {
  mqtt_broker = process.env['MQTT_BROKER'];
}
const client = mqtt.connect(mqtt_broker);

Prometheus.collectDefaultMetrics({timeout: 5000});

const zwaveNodeAliasMap = {
  'nodeID_19' : 'Gree HVAC power'
}

const zigbee_battery = new Prometheus.Gauge({
  name: 'zigbee_battery',
  help: 'zigbee battery status',
  labelNames: ['mqtt', 'name']
});
const zigbee_state = new Prometheus.Gauge({
  name: 'zigbee_state',
  help: 'zigbee state',
  labelNames: ['mqtt', 'name']
});
const zigbee_brightness = new Prometheus.Gauge({
  name: 'zigbee_brightness',
  help: 'zigbee brightness value',
  labelNames: ['mqtt', 'name']
});
const zigbee_linkquality = new Prometheus.Gauge({
  name: 'zigbee_linkquality',
  help: 'zigbee linkquality status',
  labelNames: ['mqtt', 'name']
});
const iot_temperature = new Prometheus.Gauge({
  name: 'iot_temperature',
  help: 'general iot temperature measurement',
  labelNames: ['mqtt', 'name']
});
const iot_pressure = new Prometheus.Gauge({
  name: 'iot_pressure',
  help: 'general iot pressure measurement',
  labelNames: ['mqtt', 'name']
});
const iot_humidity = new Prometheus.Gauge({
  name: 'iot_humidity',
  help: 'general iot humidity measurement',
  labelNames: ['mqtt', 'name']
});
const ruuvi_battery = new Prometheus.Gauge({
  name: 'ruuvi_battery',
  help: 'ruuvi battery measurement',
  labelNames: ['mqtt', 'name']
});
const ruuvi_humidity = new Prometheus.Gauge({
  name: 'ruuvi_humidity',
  help: 'ruuvi humidity measurement',
  labelNames: ['mqtt', 'name']
});
const ruuvi_rssi = new Prometheus.Gauge({
  name: 'ruuvi_rssi',
  help: 'ruuvi rssi measurement',
  labelNames: ['mqtt', 'name']
});
const ruuvi_movement = new Prometheus.Gauge({
  name: 'ruuvi_movement',
  help: 'ruuvi movement measurement',
  labelNames: ['mqtt', 'name']
});
const ruuvi_count = new Prometheus.Counter({
  name: 'ruuvi_count',
  help: 'count of ruuvi measurements',
  labelNames: ['mqtt', 'name']
});
const miflora_moisture = new Prometheus.Gauge({
  name: 'miflora_moisture',
  help: 'miflora moisture measurement',
  labelNames: ['mqtt', 'name']
});
const miflora_conductivity = new Prometheus.Gauge({
  name: 'miflora_conductivity',
  help: 'miflora conductivity measurement',
  labelNames: ['mqtt', 'name']
});
const miflora_light = new Prometheus.Gauge({
  name: 'miflora_light',
  help: 'miflora light measurement',
  labelNames: ['mqtt', 'name']
});
const miflora_battery = new Prometheus.Gauge({
  name: 'miflora_battery',
  help: 'miflora battery measurement',
  labelNames: ['mqtt', 'name']
});
const zigbee2mqtt_action = new Prometheus.Counter({
  name: 'zigbee2mqtt_action',
  help: 'zigbee2mqtt action counters',
  labelNames: ['id', 'action', 'name']
});
const iot_airquality = new Prometheus.Gauge({
  name: 'iot_airquality',
  help: 'general iot air quality measurement',
  labelNames: ['mqtt', 'name', 'type']
});


const goecharger_rbt = new Prometheus.Gauge({
  name: 'goecharger_rbt',
  help: 'goecharger reboot counter',
  labelNames: ['id']
});
const goecharger_rssi = new Prometheus.Gauge({
  name: 'goecharger_rssi',
  help: 'goecharger rssi',
  labelNames: ['id']
});
const goecharger_car = new Prometheus.Gauge({
  name: 'goecharger_car',
  help: 'goecharger car present',
  labelNames: ['id']
});
const goecharger_err = new Prometheus.Gauge({
  name: 'goecharger_err',
  help: 'goecharger error code',
  labelNames: ['id']
});
const goecharger_modelStatus = new Prometheus.Gauge({
  name: 'goecharger_modelStatus',
  help: 'goecharger modelStatus',
  labelNames: ['id']
});
const goecharger_acu = new Prometheus.Gauge({
  name: 'goecharger_acu',
  help: 'goecharger How many ampere is the car allowed to charge now',
  labelNames: ['id']
});
const goecharger_eto = new Prometheus.Gauge({
  name: 'goecharger_eto',
  help: 'goecharger energy_total, measured in Wh',
  labelNames: ['id']
});
const goecharger_etop = new Prometheus.Gauge({
  name: 'goecharger_etop',
  help: 'goecharger energy_total persisted, measured in Wh, without the extra magic to have live values',
  labelNames: ['id']
});
const goecharger_nrg_current = new Prometheus.Gauge({
  name: 'goecharger_nrg_current',
  help: 'goecharger current amperage from the nrg array',
  labelNames: ['id', 'phase']
});
const goecharger_nrg_voltage = new Prometheus.Gauge({
  name: 'goecharger_nrg_voltage',
  help: 'goecharger phase voltages from the nrg array',
  labelNames: ['id', 'phase']
});
const nest_ivt_register = new Prometheus.Gauge({
  name: 'nest_ivt_register',
  help: 'nest IVT heat pump register values',
  labelNames: ['mqtt', 'name']
});
const iot_power = new Prometheus.Gauge({
  name: 'iot_power',
  help: 'iot power measurement in Watts',
  labelNames: ['mqtt', 'name']
});
const iot_energy_consumption = new Prometheus.Gauge({
  name: 'iot_energy_consumption',
  help: 'iot energy consumption measurement in kWh',
  labelNames: ['mqtt', 'name']
});
const iot_battery_voltage = new Prometheus.Gauge({
  name: 'iot_battery_voltage',
  help: 'iot battery voltage in V',
  labelNames: ['mqtt', 'name']
});
const iot_battery_percentage = new Prometheus.Gauge({
  name: 'iot_battery_percentage',
  help: 'iot battery percentage in percents',
  labelNames: ['mqtt', 'name']
});
const lora_rssi = new Prometheus.Gauge({
  name: 'lora_rssi',
  help: 'lora received RSSI',
  labelNames: ['mqtt', 'name']
});
const nest_wastestorage_depth = new Prometheus.Gauge({
  name: 'nest_wastestorage_depth',
  help: 'nest wastestorage depth',
  labelNames: ['mqtt']
});

const zigbee2mqtt_bridge_online = new Prometheus.Gauge({
  name: 'zigbee2mqtt_bridge_online',
  help: 'Tracks zigbee2mqtt/bridge/state and writes 1 for online and 0 for offline',
  labelNames: ['mqtt']
});

const p1reader = new Prometheus.Gauge({
  name: 'p1reader',
  help: 'Tracks power meter values from p1reader',
  labelNames: ['mqtt', 'name', 'sensor']
});

const matched_topics = {};
const not_matched_topics = {};

const ruuvi_gateway_locations = {
  "F5:CF:35:D8:DE:EC" : "harbonkatu",
};

const nicknames = {
  "harbonkatu/ruuvi/e7bf50d26f2d" : 'Lastenhuone',
  "harbonkatu/ruuvi/f6be9b220499" : 'Olohuone',
  "harbonkatu/ruuvi/c19fc03e66d6" : 'Terassi',
  "harbonkatu/ruuvi/e702b864bab3" : 'Kylpyhuone',
  "harbonkatu/ruuvi/dcb4fd9428a7" : 'Makuuhuone',
  "harbonkatu/ruuvi/fdc42ee55323" : 'Garon työhuone',
  "harbonkatu/ruuvi/e8ec2d5b3e5f" : 'Pupu',
  "zigbee2mqtt/0x000b57fffe1175e6/action" : 'Ikea painonappi',
  "nest/ruuvi/e8ec2d5b3e5f" : 'Pupu',
  "nest/ruuvi/cd3fbdb73a15" : 'xx',
  "nest/ruuvi/ef900e053608" : 'Mökki alakerta',
  "nest/ruuvi/f79dc65ac34d" : 'Mökki yläkerta',
  "zigbee2mqtt/0x943469fffe7c12fc" : 'Air Quality (Vindstyrka)',
};

const rules = [
  {
    topic: "harbonkatu/ruuvi/#",
    regex: /^harbonkatu\/ruuvi\/(.+)/,
    fn : function(topic, matches, message) {
      const val = JSON.parse(message);
      const name = (nicknames[topic] || '');

      // Dont accept unknown tags
      if (name == '') {
        return;
      }
      iot_temperature.labels(topic, name).set(val.temperature);
      iot_pressure.labels(topic, name).set(val.pressure);
      iot_humidity.labels(topic, name).set(val.humidity);
      ruuvi_battery.labels(topic, name).set(val.battery);
      ruuvi_humidity.labels(topic, name).set(val.humidity);
      ruuvi_rssi.labels(topic, name).set(val.rssi);
      ruuvi_count.labels(topic, name).inc(1)
    }
  },
  {
    topic: "harbonkatu/miflora/#",
    regex: /^harbonkatu\/miflora\/(.+)/,
    fn : function(topic, matches, message) {
      const val = JSON.parse(message);
      const plant = matches[1];
      if (plant == '$announce') {
        return;
      }
      iot_temperature.labels(topic, plant).set(val.temperature);
      miflora_moisture.labels(topic, plant).set(val.moisture);
      miflora_conductivity.labels(topic, plant).set(val.conductivity);
      miflora_light.labels(topic, plant).set(val.light);
      miflora_battery.labels(topic, plant).set(val.battery);
    }
  },
  {
    topic: "zigbee2mqtt/#",
    regex: /^zigbee2mqtt\/(.+)\/action/,
    fn : function(topic, matches, message) {
      const id = matches[1];
      const action = message;
      const name = (nicknames[topic] || '');
      zigbee2mqtt_action.labels(id, action, name).inc(1);
    }
  },
  {
    topic: "zigbee2mqtt/#",
    regex: /^zigbee2mqtt\/([^\/]+)$/,
    fn : function(topic, matches, message) {
      const id = matches[1];
      const name = (nicknames[topic] || '');
      try {
        const fields = JSON.parse(message);
        //console.log("TEST: ", fields);
        // iterate over each key => value pair
        for (const [key, value] of Object.entries(fields)) {
          if (key == "linkquality" && value != null) {
            zigbee_linkquality.labels(id, name).set(value);
          }

          if (key == "battery" && value != null) {
            zigbee_battery.labels(id, name).set(value);
          }

          if (key == "state" && value != null) {
            var v = -1;
            if (value.toLowerCase() == "on" || value.toLowerCase() == "true" || value.toLowerCase() == "yes") {
              v = 1;
            } else if (value.toLowerCase() == "off" || value.toLowerCase() == "false" || value.toLowerCase() == "no") {
              v = 0;
            }
            zigbee_state.labels(id, name).set(v);
          }

          if (key == "brightness" && value != null) {
            zigbee_brightness.labels(id, name).set(value);
          }

          if (key == "humidity" && value != null) {
            iot_humidity.labels(id, name).set(value);
          }

          if (key == "temperature" && value != null) {
            iot_humidity.labels(id, name).set(value);
          }
        }
      } catch (e) {
        console.error("Error: ", e);
      }
    }
  },
  {
    topic: "nest/ruuvi/#",
    regex: /^nest\/ruuvi\/(.+)/,
    fn : function(topic, matches, message) {
      const val = JSON.parse(message);
      const name = (nicknames[topic] || '');
      iot_temperature.labels(topic, name).set(val.temperature);
      iot_pressure.labels(topic, name).set(val.pressure);
      iot_humidity.labels(topic, name).set(val.humidity);
      ruuvi_battery.labels(topic, name).set(val.battery);
      ruuvi_humidity.labels(topic, name).set(val.humidity);
      ruuvi_rssi.labels(topic, name).set(val.rssi);
    }
  },
  {
    topic: "zigbee2mqtt/0x943469fffe7c12fc",
    regex: /^zigbee2mqtt\/0x943469fffe7c12fc/,
    fn : function(topic, matches, message) {
      const val = JSON.parse(message);
      const name = (nicknames[topic] || '');
      iot_temperature.labels(topic, name).set(val.temperature);
      iot_humidity.labels(topic, name).set(val.humidity);
      iot_airquality.labels(topic, name, 'pm25').set(val.pm25);
      iot_airquality.labels(topic, name, 'voc_index').set(val.voc_index);
    }
  },
  {
    topic: "nest/ivt/#",
    regex: /^nest\/ivt\/(.+)/,
    fn : function(topic, matches, message) {
      const name = matches[1];
      try {
        const msg = JSON.parse(message);
        const value = msg.value;
        
        //console.log("DEBUG:, value, name, topic, Number(value));
  
        // Ensure value is a number
        if (value === "" || isNaN(value)) {
          console.log("ERR:", value, name, topic, "is not ok", value === "", isNaN(value), typeof value);
          return;
        }
        nest_ivt_register.labels(topic, name).set(Number(value));  
      } catch (e) {
        if (message != null) {
          const m = message.toString().toLowerCase();
          var v = -1;
          if (m == "OFF" || message == "false" || message == "no") {
            v = 0;
          } else if (message == "ON" || message == "true" || message == "yes") {
            v = 1;
          }
          nest_ivt_register.labels(topic, name).set(v);
        }
      }
    }
  },
  {
    topic: "nest/power/current",
    regex: /^nest\/power\/current/,
    fn : function(topic, matches, message) {
      const msg = JSON.parse(message);
      const value = msg.value;

      iot_power.labels(topic, "nest power usage").set(Number(value));
    }
  },
  {
    topic: "/go-eCharger/059956/#",
    regex: /^\/go-eCharger\/(.+)\/(.+)/,
    fn : function(topic, matches, message) {
      const id = matches[1];
      const code = matches[2];
      let value = parseInt(message, 10);
      if (message == "null") {
        value = -1;
      }
      if (code == "rbt") {
        goecharger_rbt.labels(id).set(value);
      }
      if (code == "rssi") {
        goecharger_rssi.labels(id).set(value);
      }
      if (code == "car") {
        goecharger_car.labels(id).set(value);
      }
      if (code == "modelStatus") {
        goecharger_modelStatus.labels(id).set(value);
      }
      if (code == "acu") {
        goecharger_acu.labels(id).set(value);
      }
      if (code == "eto") {
        goecharger_eto.labels(id).set(value);
      }
      if (code == "etop") {
        goecharger_etop.labels(id).set(value);
      }
      if (code == "nrg") {
        const arr = JSON.parse(message);
        goecharger_nrg_voltage.labels(id, 1).set(arr[0]);
        goecharger_nrg_voltage.labels(id, 2).set(arr[1]);
        goecharger_nrg_voltage.labels(id, 3).set(arr[2]);
        goecharger_nrg_current.labels(id, 1).set(arr[4]);
        goecharger_nrg_current.labels(id, 2).set(arr[5]);
        goecharger_nrg_current.labels(id, 3).set(arr[6]);
      }
    }
  },
  {
    topic: "koti/zwave/#",
    // harbonkatu/zwave/nodeID_19/sensor_multilevel/endpoint_0/Power
    regex: /.*\/zwave\/(.*)\/sensor_multilevel\/endpoint_0\/Power/,
    fn : function(topic, matches, message) {
      const msg = JSON.parse(message);
      const value = msg.value;
      const node = matches[1];
      const name = zwaveNodeAliasMap[node] != undefined ? zwaveNodeAliasMap[node] : node;

      iot_power.labels(topic, name).set(Number(value));
    }
  },     
  {
    topic: "koti/zwave/#",
    // harbonkatu/zwave/nodeID_19/sensor_multilevel/endpoint_0/Power
    regex: /.*\/zwave\/(.*)\/meter\/endpoint_0\/value\/66049/,
    fn : function(topic, matches, message) {
      const msg = JSON.parse(message);
      const value = msg.value;
      const node = matches[1];
      const name = zwaveNodeAliasMap[node] != undefined ? zwaveNodeAliasMap[node] : node;

      iot_power.labels(topic, name).set(Number(value));
    }
  },
  {
    topic: "koti/zwave/#",
    // harbonkatu/zwave/nodeID_19/meter/endpoint_0/value/65537
    regex: /.*\/zwave\/(.*)\/meter\/endpoint_0\/value\/65537/,
    fn : function(topic, matches, message) {
      const msg = JSON.parse(message);
      const value = msg.value;
      const node = matches[1];
      console.log("Got node id", node);
      const name = zwaveNodeAliasMap[node] != undefined ? zwaveNodeAliasMap[node] : node;
      iot_energy_consumption.labels(topic, name).set(Number(value));
    }
  },
  {
    topic: "nest/saunatemp/sauna",
    regex: /nest\/saunatemp\/sauna/,
    fn : function(topic, matches, message) {
      iot_temperature.labels(topic, "sauna").set(Number(message));
    }
  },
  {
    topic: "nest/wastestorage/depth",
    regex: /nest\/wastestorage\/depth/,
    fn : function(topic, matches, message) {
      nest_wastestorage_depth.labels(topic).set(Number(message));
    }
  },
  {
    topic: "ruuvi/#",
    // ruuvi/F5:CF:35:D8:DE:EC/E7:BF:50:D2:6F:2D
    // payload: { "data": "02010611FF990403501600CAB00005FFE903F30B5F", ... }
    regex: /ruuvi\/(.*)\/(.+)/,
    fn : function(topic, matches, message) {
      const msg = JSON.parse(message);
      const data = msg.data;

      const gateway_mac = matches[1];
      if (!matches[2]) {
        return;
      }

      const source_mac = matches[2].toLowerCase().replaceAll(":","");
      if (source_mac == "gw_status") {
        return;
      }

      const location = ruuvi_gateway_locations[gateway_mac];
      if (!location) {
        return;
      }
      const simulated_mqtt_topic = location + "/ruuvi/" + source_mac;
      const name = (nicknames[simulated_mqtt_topic] || '');

      const result = ruuviGateway.parse(data);
      if (result == null) {
        return;
      }
      if (result.temperature != undefined) {
        iot_temperature.labels(simulated_mqtt_topic, name).set(result.temperature);
      }
      if (result.pressure != undefined) {
        iot_pressure.labels(simulated_mqtt_topic, name).set(result.pressure);
      }
      if (result.humidity != undefined) {
        iot_humidity.labels(simulated_mqtt_topic, name).set(result.humidity);
      }
      if (result.battery != undefined) {
        ruuvi_battery.labels(simulated_mqtt_topic, name).set(result.battery);
      }
      if (result.movementCounter != undefined) {
        ruuvi_movement.labels(simulated_mqtt_topic, name).set(result.movementCounter);
      }
      if (msg.rssi != undefined) {
        ruuvi_rssi.labels(simulated_mqtt_topic, name).set(msg.rssi);
      }
      ruuvi_count.labels(topic, name).inc(1);
    }
  },     
  {
    topic: "lora/#",
    // ruuvi/F5:CF:35:D8:DE:EC/E7:BF:50:D2:6F:2D
    // payload: { "data": "02010611FF990403501600CAB00005FFE903F30B5F", ... }
    regex: /lora\/(.*)\/(.+)/,
    fn : function(topic, matches, message) {
      const name = matches[1];

      if (matches[2] == "rssi") {
        lora_rssi.labels(topic, name).set(parseInt(message, 10));
        return;
      }

      if (matches[2] != "payload") {
        return;
      }

      // {"vbat":4.18,"pbat":98,"ti":45.7,"t1":14.5,"t2":15.5}
      const msg = JSON.parse(message);

      if (msg.t1 != undefined) {
        iot_temperature.labels(topic, name + "_t1").set(msg.t1);
      }
      if (msg.t2 != undefined) {
        iot_temperature.labels(topic, name + "_t2").set(msg.t2);
      }
      if (msg.ti != undefined) {
        iot_temperature.labels(topic, name + "_ti").set(msg.ti);
      }
      if (msg.vbat != undefined) {
        iot_battery_voltage.labels(topic, name).set(msg.vbat);
      }
      if (msg.pbat != undefined) {
        iot_battery_percentage.labels(topic, name).set(msg.pbat);
      }
    }
  },     
  {
    topic: "zigbee2mqtt/bridge/state",
    regex: /zigbee2mqtt\/bridge\/state/,
    fn : function(topic, matches, message) {
      if (message == "online") {
        zigbee2mqtt_bridge_online.labels(topic).set(1);
      } else {
        zigbee2mqtt_bridge_online.labels(topic).set(0);
      }
    }
  },
  {
    topic: "nest/p1reader/sensor/#",
    regex: /nest\/p1reader\/sensor\/(.+?)\/state/,
    fn : function(topic, matches, message) {
      const sensor = matches[1];
      p1reader.labels(topic, "nest", sensor).set(Number(message));

    }
  },
];

client.on('connect', function () {
  for (const rule of rules) {
    console.log(rule);
    client.subscribe(rule.topic, function (err) {
      if (!err) {
        console.log("Subscribed to", rule.topic);
      }
    });
  }
})
 
client.on('message', function (topic, message) {
  let str = message.toString();

  let matched = false; // Used to keep track if message did match for logging purposes

  for (const rule of rules) {
    const found = topic.match(rule.regex);
    if (found) {
      try {
        rule.fn(topic, found, message);
      } catch (e) {
        console.error("Error calling fn for", found);
        console.log("topic:", topic, "message:", message.toString());
        console.error(e);
      }

      // Log once what kind of message matched
      matched = true;
      if (!matched_topics[topic]) {
        matched_topics[topic] = message;
        console.log("New match on topic", topic, "with rule", rule);
        console.log("The message was", str);
      }
    }
  }

  // Log once if a message did not match any rule
  if (!matched) {
    if (!not_matched_topics[topic]) {
      not_matched_topics[topic] = message;
      console.log("New topic which did not match any rules:", topic, "with message", str);
    }
  }

});

app.get('/metrics', function(req, res) {
  res.type(Prometheus.register.contentType);
  res.send(Prometheus.register.metrics());
});
app.listen(port, () => console.log(`Example app listening on port ${port}!`))



