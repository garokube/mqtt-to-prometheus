FROM node:17

ADD . /app
WORKDIR /app
RUN npm install

CMD ["node", "main.js"]
