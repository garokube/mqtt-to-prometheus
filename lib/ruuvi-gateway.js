import ruuviParser from 'ruuvi.endpoints.js';

const hexStringToByte = function (str) {
    if (!str) {
      return new Uint8Array();
    }
  
    const a = [];
    for (let i = 0, len = str.length; i < len; i += 2) {
      a.push(parseInt(str.substr(i, 2), 16));
    }
  
    return new Uint8Array(a);
};

const byteToHexString = function (uint8arr) {
    if (!uint8arr) {
      return '';
    }
  
    let hexStr = '';
    for (let i = 0; i < uint8arr.length; i++) {
      let hex = (uint8arr[i] & 0xff).toString(16);
      hex = (hex.length === 1) ? '0' + hex : hex;
      hexStr += hex;
    }
  
    return hexStr.toUpperCase();
};

const parseRuuviGatewayMessage = function(hexstring) {
    const i = hexstring.indexOf('FF99040');
    if (i == -1) {
        return null;
    }
    const binary = hexStringToByte(hexstring.slice(i + 6));
    if (binary[0] < 2 || binary[0] > 5 || binary.size < 10) {
        return null;
    }

    const data = ruuviParser.parse(binary);
    return data;
}

export default {
  parse : parseRuuviGatewayMessage,
}