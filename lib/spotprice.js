
import got from 'got';
import Prometheus from 'prom-client';

import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc.js';
import timezone from 'dayjs/plugin/timezone.js';
import isBetween from 'dayjs/plugin/isBetween.js';
dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.extend(isBetween);

let spotPrices = null;

const electricity_spot_price = new Prometheus.Gauge({
    name: 'electricity_spot_price',
    help: 'Displays electricity spot price with tax'
  });

const electricity_spot_price_ok = new Prometheus.Gauge({
    name: 'electricity_spot_price_ok',
    help: 'Shows 1 if spot price was fetched correctly'
  });

  
const refreshData = async () => {
    const ts = dayjs().tz('Europe/Helsinki');
    const date = ts.format('YYYY-MM-DD');
    const url = 'https://dataportal-api.nordpoolgroup.com/api/DayAheadPrices?date=' + date + '&market=DayAhead&deliveryArea=FI&currency=EUR';
    
    /*
        {
            "market" : "DayAhead",
            "multiAreaEntries" : [
                 { deliveryStart: "2024-10-28T23:00:00Z", deliveryEnd: "2024-10-29T00:00:00Z", entryPerArea: { "FI" : 2.17 } },
            ]
        }
    */
    try {
        const response = await got(url);

        const data = JSON.parse(response.body);
        if (data['multiAreaEntries'] == null) {
            throw new Error("spot price data did not contain multiAreaEntries struct");
        }
        spotPrices = data['multiAreaEntries'];
    } catch (e) {
        spotPrices = null; // Reset if we could not obtain it
        console.log("Error while fetching spot prices");
        console.error(e);
    }
    setTimeout(refreshData, 1000 * 60 * 66);
    return spotPrices;
}

const findCurrentSpotPrice = () => {
    if (spotPrices == null) {
        return null;
    }

    const ts = dayjs();
    for (let i in spotPrices) {
        const k = spotPrices[i];
        const startMoment = dayjs(k['deliveryStart']);
        const endMoment = dayjs(k['deliveryEnd']);
        if (ts.isBetween(startMoment, endMoment, '[)')) {
            //console.log("best match", startMoment.format(), endMoment.format(), k['entryPerArea']['FI']);
            return k['entryPerArea']['FI'];
        }
    }
    return null;
}


console.log(await refreshData());
console.log("spot data prefetch done");

function calculateEndUserPrice(wholesaleprice) {
    const ts = dayjs().tz("Europe/Helsinki");
    
    // https://www.vero.fi/tietoa-verohallinnosta/uutishuone/uutiset/uutiset/2022/sahkon-arvonlisaveroa-alennetaan-valiaikaisesti/
    if (ts.isBetween(dayjs('2022-12-01'), dayjs('2023-05-01'), '[)')) {
        return (wholesaleprice / 1000.0) * 1.10;
    } else {
        return (wholesaleprice / 1000.0) * 1.24;
    }
}

(function me() {
    const price = findCurrentSpotPrice();
    //console.log("current price", price);
    if (price != null) {
        electricity_spot_price.set(calculateEndUserPrice(price));
        electricity_spot_price_ok.set(1);
    } else {
        electricity_spot_price_ok.set(0);
    }

    setTimeout(me, 1000*15);
})();

export default () => {
    return spotPrices;
}


